.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:      Prints this screen"
	@echo "    check-fmt: Checks the formatting of the crate"
	@echo "    fmt:       Formats the crate"
	@echo "    lint:      Lints the crate"
	@echo "    build:     Build the crate"
	@echo "    test:      Runs the test for the crate"
	@echo "    bench:     Runs the benchmark tests for the crate"
	@echo "    clean:     Clean out temporaries"
	@echo ""

.PHONY: check-fmt
check-fmt:
	@echo "Checking the formatting of the crate"
	cargo fmt --all --check

.PHONY: fmt
fmt:
	@echo "Auto Formatting"
	cargo fmt --all

.PHONY: lint
lint:
	@echo "Linting"
	cargo clippy

.PHONY: build
build:
	@echo "Building the crate"
	cargo build

.PHONY: test
test:
	@echo "Run the test for the crate"
	cargo test

.PHONY: bench
bench:
	@echo "Run the benchmark tests for the crate"
	cargo bench

.PHONY: clean
clean:
	@echo "Removing temporary files"
	cargo clean
