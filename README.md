# bheap-rs

A crate to implement Poul-Henning Kamp's `bheap` in Rust.

https://queue.acm.org/detail.cfm?id=1814327

## Local Development

A `Makefile` can be used to handle any operations in the repo.

```
Usage:
    help:      Prints this screen
    check-fmt: Checks the formatting of the crate
    fmt:       Formats the crate
    lint:      Lints the crate
    build:     Build the crate
    test:      Runs the test for the crate
    bench:     Runs the benchmark tests for the crate
    clean:     Clean out temporaries
```

## Data Structure Layout

Unlike a normal `heap` that follows the following standard layout:

![Heap](diagrams/heap.svg)

The `bheap` uses a pattern that takes advantage of the local cache strucuture to avoid page misses.

![BHeap](diagrams/bheap.svg)
